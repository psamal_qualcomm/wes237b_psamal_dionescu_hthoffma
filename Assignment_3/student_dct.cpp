#include "main.h"


using namespace cv;

//declarations LUTs wrt width, height; definitions made elsewhere below
//LUT_w and LUT_h are external variables for functions from this point on,
//to end of this file (ibid. re. scope); not visible in other files main.cpp, etc
Mat LUT_w;
Mat LUT_h;
#define min(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a < _b ? _a : _b; })


// Helper function
float sf(int in){
	if (in == 0)
		return 0.70710678118; // = 1 / sqrt(2)
	return 1.;
}

//matrix multiplication naive
//void cmm(Mat lmf, Mat rmf, float* prod_ptr, int sbs)//tested
void cmm(Mat* lmf, Mat* rmf, float* prod_ptr, int sbs)//with pointers
{//caller must insure that matrix product lmf*rmf fits into reserved space pointed to by prod_ptr
    //lmf, rmf = left, right matrix factors; their sizes are well defined, per Mat objects
    // sbs=size of sub-block (which must be square)
    int lmf_w=(*lmf).cols;
    int lmf_h=(*lmf).rows;
    int rmf_w=(*rmf).cols;
    int rmf_h=(*rmf).rows;
    assert(sbs==1);
    assert(lmf_w==rmf_h);
    assert(lmf_w%sbs==0 && lmf_h%sbs==0 &&  rmf_w%sbs==0 && rmf_h%sbs==0);
    int steps=lmf_w/sbs;
    //Mat prod=Mat(lmf_h,rmf_w,CV_32FC1);
    //float* prod_ptr  = prod.ptr<float>();
    float* lmf_ptr  = (*lmf).ptr<float>();
    float* rmf_ptr  = (*rmf).ptr<float>();

    for(int x = 0; x < lmf_h; x++) // `v,' or DCT-row-frequency-index
        {
            for(int y = 0; y < rmf_w; y++) // u or DCT-col-frequency-index
            {
                float value = 0.f;
                for(int i=0;i<steps;i++)
                {
                    value += lmf_ptr[x * lmf_w + i]//S[i,j]=S[image-row-index (ordinate, `y'),image-col-index (abscissa, `x')]
                    *rmf_ptr[i * rmf_w  + y];//Cw[j,y]=Cw[S-col-index,DCT-col-index]
                }

                prod_ptr[x * rmf_w + y] = value;//F[DCT-row-frequency-index (`v'), DCT-col-frequency-index (`u')]
            }
        }
        //return prod;
}


//block matrix multiplication old
//void bcmm(Mat lmf, Mat rmf, float* prod_ptr, int b)//tested
//void bcmm(Mat lmf, Mat rmf, Mat* prod, int b)
void bcmm(Mat* lmf, Mat* rmf, float* prod_ptr, int b)//with pointers
{//AUTHOR Dumitru Mihai Ionescu
    //2-D matrices
    //caller must insure that matrix product lmf*rmf fits into reserved space pointed to by prod_ptr
    //lmf, rmf = left, right matrix factors; their sizes are well defined, per Mat objects
    // b=size of sub-block (which must be square)
    // b need NOT divide the sizes of the factor matrices
    int A_cols=(*lmf).cols;
    int A_rows=(*lmf).rows;
    int B_cols=(*rmf).cols;
    int B_rows=(*rmf).rows;
    long double value=0.d;

    Mat temp=Mat(b,b,CV_64FC1,Scalar(0));
    double* prod_ptr_bis=temp.ptr<double>();


    assert(A_cols==B_rows);//multiplication is well-defined
    //assert(A_cols%b==0 && A_rows%b==0 &&  B_cols%b==0 && B_rows%b==0);

    float* A_ptr  = (*lmf).ptr<float>();
    float* B_ptr  = (*rmf).ptr<float>();

    for(int I = 0; I < A_rows; I+=b)
        {
            for(int J = 0; J < B_cols; J+=b)
                {
                    temp=Scalar(0.f);
                    for(int K = 0; K < A_cols; K+=b)
                        {
                            for(int i = I; i <= min((I+b-1),(A_rows-1)); i++)
                                {
                                    for(int j = J; j <= min((J+b-1),(B_cols-1)); j++)
                                        {
                                            value=(long double) 0;
                                            for(int k = K; k <= min((K+b-1),(B_rows-1)); k++)
                                                {
                                                    value+=(long double) A_ptr[i*A_cols+k]*(long double)B_ptr[k*B_cols+j];
                                                    //printf("\n\r  value= %f; k= %d\n\r",value,k);
                                                }
                                                prod_ptr_bis[(i-I)*b+(j-J)]+=( double) value;//updated with each new value of K
                                                if(A_cols-K<=b)
                                                    prod_ptr[i*B_cols+j]=(float) prod_ptr_bis[(i-I)*b+(j-J)];//updated on final value of K

                                                //printf("\n\r  prod_ptr[%d * %d+%d]= %f; K= %d, J=%d, I=%d\n\r",i,B_cols,j,value,K,J,I);

                                                //(*prod).at<float>(i,j);//using 'at' method to access
                                                ////printf("\n\r  (*prod).at<float>(i,j)= %f; K= %d, J=%d, I=%d\n\r",i,B_cols,j,value,K,J,I);
                                        }
                                }
                        }
                }
        }

}






void bcmmt(Mat* lmf, Mat* rmf, float* prod_ptr, int ib, int kb)//with pointers
{//AUTHOR Dumitru Mihai Ionescu
    //2-D matrices, tiling
    //caller must insure that matrix product lmf*rmf fits into reserved space pointed to by prod_ptr
    //lmf, rmf = left, right matrix factors; their sizes are well defined, per Mat objects
    // ib, kb=row and column sizes of sub-block tiles
    // ib, kb need NOT divide the respective sizes of the factor matrices
    int A_cols=(*lmf).cols;
    int A_rows=(*lmf).rows;
    int B_cols=(*rmf).cols;
    int B_rows=(*rmf).rows;
     double value=0.d, acc_buff=0.d;

    Mat temp=Mat(ib,kb,CV_32FC1,Scalar(0));
    float* prod_ptr_bis=temp.ptr<float>();

    assert(A_cols==B_rows);//multiplication is well-defined
    //assert(A_cols%b==0 && A_rows%b==0 &&  B_cols%b==0 && B_rows%b==0);

    float* A_ptr  = (*lmf).ptr<float>();
    float* B_ptr  = (*rmf).ptr<float>();

    for(int I = 0; I < A_rows; I+=ib)
        {
            for(int K = 0; K < A_cols; K+=kb)
                {
                    for(int j = 0; j < B_cols; j+=1)
                        {
                            for(int i = I; i <= min((I+ib-1),(A_rows-1)); i++)
                                {
                                    if(K==0)
                                        acc_buff=0.d;
                                    else
                                        acc_buff=prod_ptr[i*B_cols+j];

                                    for(int k = K; k <= min((K+kb-1),(B_rows-1)); k++)
                                        {
                                            acc_buff+=( float) A_ptr[i*A_cols+k]*( float)B_ptr[k*B_cols+j];
                                            //printf("\n\r  value= %f; k= %d\n\r",value,k);
                                        }
                                        prod_ptr[i*B_cols+j]=(float) acc_buff;
                                }
                        }
                }
        }
}


///////////////////////////////




















/*
///////////////////////////////FORMER VERSION/////////////////////////////////////
void bcmmt(Mat* lmf, Mat* rmf, float* prod_ptr, int btr, int btc)//with pointers
{//AUTHOR Dumitru Mihai Ionescu
    //2-D matrices, tiling
    //caller must insure that matrix product lmf*rmf fits into reserved space pointed to by prod_ptr
    //lmf, rmf = left, right matrix factors; their sizes are well defined, per Mat objects
    // btr, btc=row and column sizes of sub-block tiles
    // btr, btc need NOT divide the respective sizes of the factor matrices
    int A_cols=(*lmf).cols;
    int A_rows=(*lmf).rows;
    int B_cols=(*rmf).cols;
    int B_rows=(*rmf).rows;
     double value=0.d;

    Mat temp=Mat(btr,btc,CV_32FC1,Scalar(0));
    float* prod_ptr_bis=temp.ptr<float>();


    assert(A_cols==B_rows);//multiplication is well-defined
    //assert(A_cols%b==0 && A_rows%b==0 &&  B_cols%b==0 && B_rows%b==0);

    float* A_ptr  = (*lmf).ptr<float>();
    float* B_ptr  = (*rmf).ptr<float>();

    for(int I = 0; I < A_rows; I+=btr)
        {
            for(int J = 0; J < B_cols; J+=btc)
                {
                    temp=Scalar(0.f);
                    for(int K = 0; K < A_cols; K+=btc)
                        {
                            for(int i = I; i <= min((I+btr-1),(A_rows-1)); i++)
                                {
                                    for(int j = J; j <= min((J+btc-1),(B_cols-1)); j++)
                                        {
                                            value=( double) 0;
                                            for(int k = K; k <= min((K+btc-1),(B_rows-1)); k++)
                                                {
                                                    value+=( float) A_ptr[i*A_cols+k]*( float)B_ptr[k*B_cols+j];
                                                    //printf("\n\r  value= %f; k= %d\n\r",value,k);
                                                }
                                                prod_ptr_bis[(i-I)*btc+(j-J)]+=( double) value;//updated with each new value of K
                                                if(A_cols-K<=btc)
                                                    prod_ptr[i*B_cols+j]=(float) prod_ptr_bis[(i-I)*btc+(j-J)];//updated on final value of K

                                                //printf("\n\r  prod_ptr[%d * %d+%d]= %f; K= %d, J=%d, I=%d\n\r",i,B_cols,j,value,K,J,I);

                                                //(*prod).at<float>(i,j);//using 'at' method to access
                                                ////printf("\n\r  (*prod).at<float>(i,j)= %f; K= %d, J=%d, I=%d\n\r",i,B_cols,j,value,K,J,I);
                                        }
                                }
                        }
                }
        }

}


///////////////////////////////END FORMER VERSION/////////////////////////////////////
*/


//4x4 block matrix multiplication, with loop unrolling
//void bcmmlur4(Mat lmf, Mat rmf, float* prod_ptr, int b)//tested
//void bcmm(Mat lmf, Mat rmf, Mat* prod, int b)
void   bcmmlur4(Mat* lmf, Mat* rmf, float* prod_ptr, int b)//with pointers
{//AUTHOR Dumitru Mihai Ionescu
    //2-D matrices, assumed to be float

    //caller must insure that matrix product lmf*rmf fits into space reserved and pointed-to by prod_ptr
    //lmf, rmf = left, right matrix factors; their sizes are well-defined, per Mat objects
    // b=size of sub-block (which must be square)=4
    // b need NOT divide the sizes of the factor matrices
    int A_cols=(*lmf).cols;
    int A_rows=(*lmf).rows;
    int B_cols=(*rmf).cols;
    int B_rows=(*rmf).rows;
    long double value=0.f;

    Mat temp=Mat(b,b,CV_64FC1,Scalar(0));
    double* prod_ptr_bis=temp.ptr<double>();

    assert(b<=4 && b>0);

    assert(A_cols==B_rows);//multiplication is well-defined
    //assert(A_cols%b==0 && A_rows%b==0 &&  B_cols%b==0 && B_rows%b==0);

    float* A_ptr  = (*lmf).ptr<float>();
    float* B_ptr  = (*rmf).ptr<float>();

    for(int I = 0; I < A_rows; I+=b)
        {
            for(int J = 0; J < B_cols; J+=b)
                {
                    temp=Scalar(0.f);
                    for(int K = 0; K < A_cols; K+=b)
                        {
                            /*
                            for(int i = I; i <= min((I+b-1),A_rows-1); i++)
                                {
                                    for(int j = J; j <= min((J+b-1),(B_cols-1)); j++)
                                        {
                                            value=(float) 0;
                                            for(int k = K; k <= min((K+b-1),(B_rows-1)); k++)
                                                {
                                                    value += A_ptr[i*A_cols+k]*B_ptr[k*B_cols+j];
                                                    //printf("\n\r  value= %f; k= %d\n\r",value,k);
                                                }
                                                prod_ptr[i*B_cols+j] += value;//updated with each new value of K
                                                //printf("\n\r  prod_ptr[%d * %d+%d]= %f; K= %d, J=%d, I=%d\n\r",i,B_cols,j,value,K,J,I);

                                                //(*prod).at<float>(i,j);//using 'at' method to access
                                                ////printf("\n\r  (*prod).at<float>(i,j)= %f; K= %d, J=%d, I=%d\n\r",i,B_cols,j,value,K,J,I);
                                        }
                                }
                                */
                            //
                            value=(long double) 0.f;//I,J,K  updates trigger new iterations over i,j,k, unrolled below: i=I,j=J,k=K+0,..., K+b-1
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+0]*B_ptr[(K+0)*B_cols+J]);
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+1]*B_ptr[(K+1)*B_cols+J]);
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+2]*B_ptr[(K+2)*B_cols+J]);
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+3]*B_ptr[(K+3)*B_cols+J]);
                            prod_ptr_bis[0*b+0]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[I*B_cols+J]=( float) prod_ptr_bis[0*b+0];//updated on final value of K

                            value=(long double) 0.f;//i=I,j=J+1,k=K+0,..., K+b-1
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+0]*B_ptr[(K+0)*B_cols+J+1]);
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+1]*B_ptr[(K+1)*B_cols+J+1]);
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+2]*B_ptr[(K+2)*B_cols+J+1]);
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+3]*B_ptr[(K+3)*B_cols+J+1]);
                            prod_ptr_bis[0*b+1]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[I*B_cols+J+1]=( float) prod_ptr_bis[0*b+1];//updated on final value of K

                            value=(long double) 0.f;//i=I,j=J+2,k=K+0,..., K+b-1
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+0]*B_ptr[(K+0)*B_cols+J+2]);
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+1]*B_ptr[(K+1)*B_cols+J+2]);
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+2]*B_ptr[(K+2)*B_cols+J+2]);
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+3]*B_ptr[(K+3)*B_cols+J+2]);
                            prod_ptr_bis[0*b+2]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[I*B_cols+J+2]=( float) prod_ptr_bis[0*b+2];//updated on final value of K

                            value=(long double) 0.f;//i=I,j=J+3,k=K+0,..., K+b-1
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+0]*B_ptr[(K+0)*B_cols+J+3]);
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+1]*B_ptr[(K+1)*B_cols+J+3]);
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+2]*B_ptr[(K+2)*B_cols+J+3]);
                            value+=(long double) ((I <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[I*A_cols+K+3]*B_ptr[(K+3)*B_cols+J+3]);
                            prod_ptr_bis[0*b+3]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[I*B_cols+J+3]=( float) prod_ptr_bis[0*b+3];//updated on final value of K

                            value=(long double) 0.f;//i=I+1,j=J,k=K+0,..., K+b-1
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+0]*B_ptr[(K+0)*B_cols+J]);
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+1]*B_ptr[(K+1)*B_cols+J]);
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+2]*B_ptr[(K+2)*B_cols+J]);
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+3]*B_ptr[(K+3)*B_cols+J]);
                            prod_ptr_bis[1*b+0]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[(I+1)*B_cols+J]=( float) prod_ptr_bis[1*b+0];//updated on final value of K

                            value=(long double) 0.f;//i=I+1,j=J+1,k=K+0,..., K+b-1
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+0]*B_ptr[(K+0)*B_cols+J+1]);
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+1]*B_ptr[(K+1)*B_cols+J+1]);
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+2]*B_ptr[(K+2)*B_cols+J+1]);
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+3]*B_ptr[(K+3)*B_cols+J+1]);
                            prod_ptr_bis[1*b+1]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[(I+1)*B_cols+J+1]=( float) prod_ptr_bis[1*b+1];//updated on final value of K

                            value=(long double) 0.f;//i=I+1,j=J+2,k=K+0,..., K+b-1
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+0]*B_ptr[(K+0)*B_cols+J+2]);
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+1]*B_ptr[(K+1)*B_cols+J+2]);
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+2]*B_ptr[(K+2)*B_cols+J+2]);
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+3]*B_ptr[(K+3)*B_cols+J+2]);
                            prod_ptr_bis[1*b+2]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[(I+1)*B_cols+J+2]=( float) prod_ptr_bis[1*b+2];//updated on final value of K

                            value=(long double) 0.f;//i=I+1,j=J+3,k=K+0,..., K+b-1
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+0]*B_ptr[(K+0)*B_cols+J+3]);
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+1]*B_ptr[(K+1)*B_cols+J+3]);
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+2]*B_ptr[(K+2)*B_cols+J+3]);
                            value+=(long double) ((I+1 <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+1)*A_cols+K+3]*B_ptr[(K+3)*B_cols+J+3]);
                            prod_ptr_bis[1*b+3]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[(I+1)*B_cols+J+3]=( float) prod_ptr_bis[1*b+3];//updated on final value of K



                            value=(long double) 0.f;//i=I+2,j=J,k=K+0,..., K+b-1
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+0]*B_ptr[(K+0)*B_cols+J]);
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+1]*B_ptr[(K+1)*B_cols+J]);
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+2]*B_ptr[(K+2)*B_cols+J]);
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+3]*B_ptr[(K+3)*B_cols+J]);
                            prod_ptr_bis[2*b+0]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[(I+2)*B_cols+J]=( float) prod_ptr_bis[2*b+0];//updated on final value of K

                            value=(long double) 0.f;//i=I+2,j=J+1,k=K+0,..., K+b-1
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+0]*B_ptr[(K+0)*B_cols+J+1]);
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+1]*B_ptr[(K+1)*B_cols+J+1]);
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+2]*B_ptr[(K+2)*B_cols+J+1]);
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+3]*B_ptr[(K+3)*B_cols+J+1]);
                            prod_ptr_bis[2*b+1]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[(I+2)*B_cols+J+1]=( float) prod_ptr_bis[2*b+1];//updated on final value of K

                            value=(long double) 0.f;//i=I+2,j=J+2,k=K+0,..., K+b-1
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+0]*B_ptr[(K+0)*B_cols+J+2]);
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+1]*B_ptr[(K+1)*B_cols+J+2]);
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+2]*B_ptr[(K+2)*B_cols+J+2]);
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+3]*B_ptr[(K+3)*B_cols+J+2]);
                            prod_ptr_bis[2*b+2]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[(I+2)*B_cols+J+2]=( float) prod_ptr_bis[2*b+2];//updated on final value of K

                            value=(long double) 0.f;//i=I+2,j=J+3,k=K+0,..., K+b-1
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+0]*B_ptr[(K+0)*B_cols+J+3]);
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+1]*B_ptr[(K+1)*B_cols+J+3]);
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+2]*B_ptr[(K+2)*B_cols+J+3]);
                            value+=(long double) ((I+2 <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+2)*A_cols+K+3]*B_ptr[(K+3)*B_cols+J+3]);
                            prod_ptr_bis[2*b+3]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[(I+2)*B_cols+J+3]=( float) prod_ptr_bis[2*b+3];//updated on final value of K




                            value=(long double) 0.f;//i=I+3,j=J,k=K+0,..., K+b-1
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+0]*B_ptr[(K+0)*B_cols+J]);
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+1]*B_ptr[(K+1)*B_cols+J]);
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+2]*B_ptr[(K+2)*B_cols+J]);
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J   <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+3]*B_ptr[(K+3)*B_cols+J]);
                            prod_ptr_bis[3*b+0]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[(I+3)*B_cols+J]=( float) prod_ptr_bis[3*b+0];//updated on final value of K

                            value=(long double) 0.f;//i=I+3,j=J+1,k=K+0,..., K+b-1
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+0]*B_ptr[(K+0)*B_cols+J+1]);
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+1]*B_ptr[(K+1)*B_cols+J+1]);
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+2]*B_ptr[(K+2)*B_cols+J+1]);
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J+1 <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+3]*B_ptr[(K+3)*B_cols+J+1]);
                            prod_ptr_bis[3*b+1]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[(I+3)*B_cols+J+1]=( float) prod_ptr_bis[3*b+1];//updated on final value of K

                            value=(long double) 0.f;//i=I+3,j=J+2,k=K+0,..., K+b-1
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+0]*B_ptr[(K+0)*B_cols+J+2]);
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+1]*B_ptr[(K+1)*B_cols+J+2]);
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+2]*B_ptr[(K+2)*B_cols+J+2]);
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J+2 <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+3]*B_ptr[(K+3)*B_cols+J+2]);
                            prod_ptr_bis[3*b+2]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[(I+3)*B_cols+J+2]=( float) prod_ptr_bis[3*b+2];//updated on final value of K

                            value=(long double) 0.f;//i=I+3,j=J+3,k=K+0,..., K+b-1
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K   <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+0]*B_ptr[(K+0)*B_cols+J+3]);
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K+1 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+1]*B_ptr[(K+1)*B_cols+J+3]);
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K+2 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+2]*B_ptr[(K+2)*B_cols+J+3]);
                            value+=(long double) ((I+3 <= min((I+b-1),(A_rows-1)) && J+3 <= min((J+b-1),(B_cols-1)) && K+3 <= min((K+b-1),(B_rows-1)))?1:0)*(A_ptr[(I+3)*A_cols+K+3]*B_ptr[(K+3)*B_cols+J+3]);
                            prod_ptr_bis[3*b+3]+=( double) value;//updated with each new value of K
                            if(A_cols-K<=b)
                                prod_ptr[(I+3)*B_cols+J+3]=( float) prod_ptr_bis[3*b+3];//updated on final value of K



                        }//here
                }

        }
        //for(int i=0;i<A_rows; i++)
         //   for(int j=0;j<B_cols; j++)
        //        prod_ptr[i*B_cols+j]=(float)prod_ptr_bis[i*B_cols+j];


}


// Initialize LUT
void initDCT(int WIDTH, int HEIGHT)
{
    {//block below served for testing only

/*

    Mat lmf=Mat(4,4,CV_32FC1);
    Mat rmf=Mat(4,6,CV_32FC1);
    Mat prod=Mat(4,6,CV_32FC1);
    float* prod_ptr=prod.ptr<float>();
    lmf.at<float>(0,0)=3;
    lmf.at<float>(0,1)=6;
    lmf.at<float>(0,2)=1;
    lmf.at<float>(0,3)=2;
    lmf.at<float>(1,0)=0;
    lmf.at<float>(1,1)=5;
    lmf.at<float>(1,2)=5;
    lmf.at<float>(1,3)=7;
    lmf.at<float>(2,0)=1;
    lmf.at<float>(2,1)=2;
    lmf.at<float>(2,2)=1;
    lmf.at<float>(2,3)=3;
    lmf.at<float>(3,0)=-1;
    lmf.at<float>(3,1)=12;
    lmf.at<float>(3,2)=-9;
    lmf.at<float>(3,3)=3;

    rmf.at<float>(0,0)=-9;
    rmf.at<float>(0,1)=2;
    rmf.at<float>(0,2)=10;
    rmf.at<float>(0,3)=7;
    rmf.at<float>(0,4)=1;
    rmf.at<float>(0,5)=11;

    rmf.at<float>(1,0)=-1;
    rmf.at<float>(1,1)=3;
    rmf.at<float>(1,2)=1;
    rmf.at<float>(1,3)=3;
    rmf.at<float>(1,4)=5;
    rmf.at<float>(1,5)=-21;

    rmf.at<float>(2,0)=0;
    rmf.at<float>(2,1)=5;
    rmf.at<float>(2,2)=0;
    rmf.at<float>(2,3)=0;
    rmf.at<float>(2,4)=12;
    rmf.at<float>(2,5)=11;

    rmf.at<float>(3,0)=-4;
    rmf.at<float>(3,1)=0;
    rmf.at<float>(3,2)=-14;
    rmf.at<float>(3,3)=20;
    rmf.at<float>(3,4)=8;
    rmf.at<float>(3,5)=0;


    cmm(&lmf,&rmf,prod_ptr,1);

    printf("\n\r %+f %+f %+f  %+f %+f %+f \n\r %+f %+f %+f  %+f %+f %+f \n\r %+f %+f %+f  %+f %+f %+f  \n\r %+f %+f %+f  %+f %+f %+f \n\r",
           prod.at<float>(0,0),prod.at<float>(0,1),prod.at<float>(0,2),prod.at<float>(0,3),prod.at<float>(0,4),prod.at<float>(0,5),
           prod.at<float>(1,0),prod.at<float>(1,1),prod.at<float>(1,2),prod.at<float>(1,3),prod.at<float>(1,4),prod.at<float>(1,5),
           prod.at<float>(2,0),prod.at<float>(2,1),prod.at<float>(2,2),prod.at<float>(2,3),prod.at<float>(2,4),prod.at<float>(2,5),
           prod.at<float>(3,0),prod.at<float>(3,1),prod.at<float>(3,2),prod.at<float>(3,3),prod.at<float>(3,4),prod.at<float>(3,5)
           );

    prod=Scalar(0);
    printf("\n\r %+f %+f %+f  %+f %+f %+f \n\r %+f %+f %+f  %+f %+f %+f \n\r %+f %+f %+f  %+f %+f %+f  \n\r %+f %+f %+f  %+f %+f %+f \n\r",
           prod.at<float>(0,0),prod.at<float>(0,1),prod.at<float>(0,2),prod.at<float>(0,3),prod.at<float>(0,4),prod.at<float>(0,5),
           prod.at<float>(1,0),prod.at<float>(1,1),prod.at<float>(1,2),prod.at<float>(1,3),prod.at<float>(1,4),prod.at<float>(1,5),
           prod.at<float>(2,0),prod.at<float>(2,1),prod.at<float>(2,2),prod.at<float>(2,3),prod.at<float>(2,4),prod.at<float>(2,5),
           prod.at<float>(3,0),prod.at<float>(3,1),prod.at<float>(3,2),prod.at<float>(3,3),prod.at<float>(3,4),prod.at<float>(3,5)
           );

    Mat dummy=Mat(3,5,CV_32FC1,Scalar(0));
    printf("\n\r %f  %f %f  %f %f \n\r %f  %f %f  %f %f  \n\r %f  %f %f  %f %f  \n\r",dummy.at<float>(0,0),dummy.at<float>(0,1),dummy.at<float>(0,2),dummy.at<float>(0,3),dummy.at<float>(0,4),
           dummy.at<float>(1,0),dummy.at<float>(1,1),dummy.at<float>(1,2),dummy.at<float>(1,3),dummy.at<float>(1,4),
           dummy.at<float>(2,0),dummy.at<float>(2,1),dummy.at<float>(2,2),dummy.at<float>(2,3),dummy.at<float>(2,4));

           printf("fmin (100.0, 1.0)=%f\n",fmin(100.0,1.0));
           printf("fmin (-100.0, -1.0)=%f\n",fmin(-100.0,-1.0));
           printf("(macro) min (-87.0, -1.0)=%f\n",min(-87.0,-1.0));
           printf("(macro) min (187.0, 76.50)=%f\n",min(187.0, 76.50));
           printf("(macro) min (-0, 17.0)=%f\n",min(-0, 17.0));

    prod=Scalar(0.f);
    //bcmmlur4(lmf,rmf,prod_ptr,3);//
    bcmmt(&lmf,&rmf,prod_ptr,8,10);//
    printf("\n\r block-wise multiplication:\n\r %+f %+f %+f  %+f %+f %+f \n\r %+f %+f %+f  %+f %+f %+f \n\r %+f %+f %+f  %+f %+f %+f  \n\r %+f %+f %+f  %+f %+f %+f \n\n\r",
           prod.at<float>(0,0),prod.at<float>(0,1),prod.at<float>(0,2),prod.at<float>(0,3),prod.at<float>(0,4),prod.at<float>(0,5),
           prod.at<float>(1,0),prod.at<float>(1,1),prod.at<float>(1,2),prod.at<float>(1,3),prod.at<float>(1,4),prod.at<float>(1,5),
           prod.at<float>(2,0),prod.at<float>(2,1),prod.at<float>(2,2),prod.at<float>(2,3),prod.at<float>(2,4),prod.at<float>(2,5),
           prod.at<float>(3,0),prod.at<float>(3,1),prod.at<float>(3,2),prod.at<float>(3,3),prod.at<float>(3,4),prod.at<float>(3,5)
           );
   */}




	// TODO
	float SCALEh = sqrt(2./(double)HEIGHT);//sqrt(2.)/sqrt(HEIGHT);
    float SCALEw = sqrt(2./(double)WIDTH);//sqrt(2.)/sqrt(WIDTH);

	//externals defined here, of type float, but not initialized
	LUT_h=Mat(HEIGHT,HEIGHT,CV_32FC1);
	LUT_w=Mat(WIDTH,WIDTH,CV_32FC1);


	for(int i = 0; i < HEIGHT; i++) //row
    {
        float* Mi=LUT_h.ptr<float>(i);
        for(int x = 0; x < HEIGHT; x++) // column
        {
            // --- Store cos calculation by LUT ---
            //Mi[x]=(float) cos(M_PI/((float)HEIGHT)*(i+1./2.)*(float)x);
            //LUT_h.at<float>(i,x) = cos(M_PI/((float)HEIGHT)*(i+1./2.)*(float)x)*=SCALEh*sf(i);
            *(Mi+x)=(float) cos(M_PI/((float)HEIGHT)*(i+1./2.)*(float)x);
            Mi[x]*=SCALEh*sf(x);
        }
    }

    for(int i = 0; i < WIDTH; i++) //row
    {
        float* Mi=LUT_w.ptr<float>(i);
        for(int x = 0; x < WIDTH; x++) // column
        {
            // --- Store cos calculation by LUT ---
            //Mi[x]=(float) cos(M_PI/((float)WIDTH)*(i+1./2.)*(float)x);
            //LUT_w.at<float>(i, x) = cos(M_PI/((float)WIDTH)*(i+1./2.)*(float)x)*SCALEw*sf(i);
            *(Mi+x)=(float) cos(M_PI/((float)WIDTH)*(i+1./2.)*(float)x);
            Mi[x]*=SCALEw*sf(x);
        }
    }




}


/*
// Baseline: O(N^4)

Mat student_dct(Mat input)
{
	const int HEIGHT = input.rows;
	const int WIDTH  = input.cols;
	printf("HEIGHT: %d\n\r",
				HEIGHT);

	float scale = 2./sqrt(HEIGHT*WIDTH);

	Mat result = Mat(HEIGHT, WIDTH, CV_32FC1);

	// Note: Using pointers is faster than Mat.at<float>(x,y)
	// Try to use pointers for your LUT as well
	float* result_ptr = result.ptr<float>();
	float* input_ptr  = input.ptr<float>();
	float* lut_h_ptr = LUT_h.ptr<float>();
	float* lut_w_ptr = LUT_w.ptr<float>();

	for(int x = 0; x < HEIGHT; x++) // `v,' or DCT-row-frequency-index
	{
		for(int y = 0; y < WIDTH; y++) // u or DCT-col-frequency-index
		{
			float value = 0.f;

			for(int i = 0; i < HEIGHT; i++)
			{
				for(int j = 0; j < WIDTH; j++)
				{
					value += input_ptr[i * WIDTH + j]//S[i,j]=S[image-row-index (ordinate, `y'),image-col-index (abscissa, `x')]
						// TODO
						// --- Replace cos calculation by LUT ---
						//* cos(M_PI/((float)HEIGHT)*(i+1./2.)*(float)x)
						//* cos(M_PI/((float)WIDTH)*(j+1./2.)*(float)y) ;
						//* (*(LUT_h.data+LUT_h.step[0]*i+LUT_h.step[1]*x))//LUT_h(i,x)
						//* (*(LUT_w.data+LUT_w.step[0]*j+LUT_w.step[1]*y));//LUT_w(j,y)
						//* LUT_h.at<float>(i,x)
						//* LUT_w.at<float>(j,y);
						*lut_h_ptr[i * HEIGHT + x] //Ch[i,x]=Ch[S-row-index,DCT-row-index]
						*lut_w_ptr[j * WIDTH  + y];//Cw[j,y]=Cw[S-col-index,DCT-col-index]

				}

			}
			// TODO
			// --- Incorporate the scale in the LUT coefficients ---
			// --- and remove the line below ---
			//value = scale * sf(x) * sf(y) * value;


			//insert into DCT matrix F
			result_ptr[x * WIDTH + y] = value;//F[DCT-row-frequency-index (`v'), DCT-col-frequency-index (`u')]
		}
	}

	return result;
}
*/


/*
// *****************
//   Hint this naive matrix multiplication
// *****************
//
// DCT as matrix multiplication
Mat student_dct(Mat input)
{
	// -- Works only for WIDTH == HEIGHT
	//assert(input.rows == input.cols);

	// -- Matrix multiply with OpenCV
	//Mat output = LUT_w * input * LUT_w.t();//ERROR HERE AND IN assignment2 TEXT; TRANSPOSE IS ON WRONG SIDE

	int WIDTH=input.cols;
    int HEIGHT=input.rows;

	Mat output=Mat(HEIGHT,WIDTH,CV_32FC1);
	Mat temp=Mat(HEIGHT,WIDTH,CV_32FC1);

    float* temp_ptr=temp.ptr<float>();
    float* output_ptr=output.ptr<float>();


    Mat LUT_ht=LUT_h.t();

    //cmm(input,LUT_w,temp_ptr,1);
    //cmm(LUT_h.t(),temp,output_ptr,1);

    cmm(&input,&LUT_w,temp_ptr,1);//block matrix multiplication w/block size 64-by-64
    cmm(&(LUT_ht),&temp,output_ptr,1);

	// TODO
	// Replace the line above by your own matrix multiplication code
	// You can use a temp matrix to store the intermediate result

	return output;
}
*/




//
// DCT as block matrix multiplication
Mat student_dct(Mat input)
{
	// -- Works only for WIDTH == HEIGHT
	//assert(input.rows == input.cols);

	// -- Matrix multiply with OpenCV
	//Mat output = LUT_w * input * LUT_w.t(); //ERROR HERE AND IN TEXT; TRANSPOSE IS ON WRONG SIDE

	int WIDTH=input.cols;
    int HEIGHT=input.rows;

	Mat output=Mat(HEIGHT,WIDTH,CV_32FC1);
	Mat temp=Mat(HEIGHT,WIDTH,CV_32FC1);


    float* temp_ptr=temp.ptr<float>();
    float* output_ptr=output.ptr<float>();


    Mat LUT_ht=LUT_h.t();//Mat(HEIGHT,WIDTH,CV_32FC1,LUT_h.t());


    //passing objects
    //bcmm(input,LUT_w,temp_ptr,16);//block matrix multiplication w/block size 64-by-64
    //bcmm(LUT_h.t(),temp,output_ptr,16);
    //bcmmlur4(input,LUT_w,temp_ptr,4);//block matrix multiplication w/block size 64-by-64
    //bcmmlur4(LUT_h.t(),temp,output_ptr,4);

    //using Mat* to pass first two arguments
    //bcmm(&input,&LUT_w,temp_ptr,128);//block matrix multiplication w/block size 64-by-64
    //bcmm(&(LUT_ht),&temp,output_ptr,128);
    //bcmmlur4(&input,&LUT_w,temp_ptr,4);//block matrix multiplication w/block size 64-by-64
    //bcmmlur4(&(LUT_ht),&temp,output_ptr,4);
    bcmmt(&input,&LUT_w,temp_ptr,16,18);//block matrix multiplication w/block size 64-by-64
    bcmmt(&(LUT_ht),&temp,output_ptr,16,18);//around 20; or 24
    //16, 22; 16,24;18,162;[23,40 good];



    //alternative [attempt]
    //bcmm(input,LUT_w,&temp,1);//block matrix multiplication w/block size 64-by-64
    //bcmm(LUT_h.t(),temp,&output,1);

	// TODO
	// Replace the line above by your own matrix multiplication code
	// You can use a temp matrix to store the intermediate result


	return output;
}







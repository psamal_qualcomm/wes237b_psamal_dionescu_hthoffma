/*
#include "sobel.h"

using namespace std;
using namespace cv;


int sobel_kernel_x[3][3] = {
	{ 1,  0, -1},
	{ 2,  0, -2},
	{ 1,  0, -1}};


void sobel(const Mat& src, Mat& dst)
{
	// TODO
}


void sobel_unroll(const Mat& src, Mat& dst)
{
	// TODO
}

void sobel_neon(const Mat& src, Mat& dst)
{
	// TODO
}

*/


#include <arm_neon.h>
#include "sobel.h"
using namespace std;
using namespace cv;

int sobel_kernel_x[3][3] = {
	{ 1,  0, -1},
	{ 2,  0, -2},
	{ 1,  0, -1}};

	int sobel_kernel_y[3][3] = {
	{ -1,  -2, -1},
	{ 0,  0, 0},
	{ 1,  2, 1}};



static inline void sobel_xy(const Mat& src, Mat& dst, unsigned int &x, unsigned int &y)
{
	//todo, take care of edge cases
	/*
	unsigned int acc_x = 0;
	unsigned int acc_y = 0;
	for (int kernel_row = 0; kernel_row < max_kernel; kernel_row++) {
		for (int kernel_col = 0; kernel_col < max_kernel; kernel_col++) {
					pixel = borderInterpolate(,src.rows)*src.cols)
					acc_x+= pixel*sobel_kernel_x[kernel_row][kernel_col];
					acc_y+= pixel*sobel_kernel_x[kernel_col][kernel_row];
		}
	}
	*/
}


void sobel(const Mat& src, Mat& dst)
{
	int max_kernel = 3;
	uchar* data = src.data;
	uchar pixel = 0;

/*	for (int j = 0; j <src.cols; j++)
		sobel_xy(src, dst, 0 , j);
	*/
	for (int i = 1; i <src.rows-1; i++) {
		//sobel_xy(src, dst, i , 0)
		for (int j = 1; j < src.cols-1; j++) {
			int acc_x = 0;
			int acc_y = 0;
			for (int kernel_row = 0; kernel_row < max_kernel; kernel_row++) {
				for (int kernel_col = 0; kernel_col < max_kernel; kernel_col++)
				{
					pixel = data[(i+kernel_row)* src.cols+j+kernel_col];
					acc_x+= pixel*sobel_kernel_x[kernel_row][kernel_col];
					acc_y+= pixel*sobel_kernel_y[kernel_row][kernel_col];
					//acc_y+= pixel*sobel_kernel_x[kernel_col][kernel_row];
				}
			}
			unsigned int tmp = abs((acc_x + acc_y)/2);
			if (tmp > 255)
                tmp = 255;
			dst.data[(i)*src.cols + j] = tmp;
        }
		//sobel_xy(src, dst, i, src.cols);
	}
	/*	for (int j = 0; j <src.cols; j++)
			sobel_xy(src, dst, src.rows, j)
	*/

}

void sobel_unroll(const Mat& src, Mat& dst)
{

	uchar* data = src.data;
	unsigned int kernel_row, kernel_col;
	uchar pixel;
	for (int i = 1; i <src.rows-1; i++) {
		//sobel_xy(src, dst, i , 0)
		for (int j = 1; j < src.cols-1; j++) {
            int acc_x = 0;
			int acc_y = 0;
					kernel_row = 0;
					kernel_col = 0;
					pixel = data[(i+kernel_row)* src.cols+j+kernel_col];
					acc_x+= pixel*sobel_kernel_x[kernel_row][kernel_col];
					acc_y+= pixel*sobel_kernel_y[kernel_row][kernel_col];
					//acc_y  += pixel*sobel_kernel_x[kernel_col][kernel_row];

					kernel_col = 1;
					pixel = data[(i+kernel_row)* src.cols+j+kernel_col];
					acc_x+= pixel*sobel_kernel_x[kernel_row][kernel_col];
					acc_y+= pixel*sobel_kernel_y[kernel_row][kernel_col];
					//acc_y+= pixel*sobel_kernel_x[kernel_col][kernel_row];

					kernel_col = 2;
					pixel = data[(i+kernel_row)* src.cols+j+kernel_col];
					acc_x+= pixel*sobel_kernel_x[kernel_row][kernel_col];
					acc_y+= pixel*sobel_kernel_y[kernel_row][kernel_col];
					//acc_y+= pixel*sobel_kernel_x[kernel_col][kernel_row];

					kernel_row = 1;
					kernel_col = 0;
					pixel = data[(i+kernel_row)* src.cols+j+kernel_col];
					acc_x+= pixel*sobel_kernel_x[kernel_row][kernel_col];
					acc_y+= pixel*sobel_kernel_y[kernel_row][kernel_col];
					//acc_y+= pixel*sobel_kernel_x[kernel_col][kernel_row];

					kernel_col = 2;
					pixel = data[(i+kernel_row)* src.cols+j+kernel_col];
					acc_x+= pixel*sobel_kernel_x[kernel_row][kernel_col];
					acc_y+= pixel*sobel_kernel_y[kernel_row][kernel_col];
					//acc_y+= pixel*sobel_kernel_x[kernel_col][kernel_row];

					kernel_row = 2;
					kernel_col = 0;
					pixel = data[(i+kernel_row)* src.cols+j+kernel_col];
					acc_x+= pixel*sobel_kernel_x[kernel_row][kernel_col];
					acc_y+= pixel*sobel_kernel_y[kernel_row][kernel_col];
					//acc_y+= pixel*sobel_kernel_x[kernel_col][kernel_row];

					kernel_col = 1;
					pixel = data[(i+kernel_row)* src.cols+j+kernel_col];
					acc_x+= pixel*sobel_kernel_x[kernel_row][kernel_col];
					acc_y+= pixel*sobel_kernel_y[kernel_row][kernel_col];
					//acc_y+= pixel*sobel_kernel_x[kernel_col][kernel_row];

					kernel_col = 2;
					pixel = data[(i+kernel_row)* src.cols+j+kernel_col];
					acc_x+= pixel*sobel_kernel_x[kernel_row][kernel_col];
					acc_y+= pixel*sobel_kernel_y[kernel_row][kernel_col];
					//acc_y+= pixel*sobel_kernel_x[kernel_col][kernel_row];

					unsigned int tmp = (abs(acc_x) + abs(acc_y))/2;
                    if (tmp > 255)
                        tmp = 255;
                    dst.data[(i)*src.cols + j] = tmp;

		}
		//sobel_xy(src, dst, i, src.cols);
	}
	/*	for (int j = 0; j <src.cols; j++)
			sobel_xy(src, dst, src.rows, j)
	*/
}

static void inline __sobel_neon(const Mat &src, const Mat &dst, int i, int j)
{
#if 1
			uchar* data = src.data;
			int16x8_t neon_pixels;
			int16x8_t acc_x = vdupq_n_s16(0);
			int16x8_t acc_y = vdupq_n_s16(0);
			int16x8_t coef_x;
			int16x8_t coef_y;
			int kernel_row = 0;
			int kernel_col = 0;
			int pixel_index = ((i+kernel_row) * src.cols) +j+kernel_col;
			neon_pixels = vreinterpretq_s16_u16(vmovl_u8(vld1_u8(data + pixel_index)));
			coef_x = vdupq_n_s16(sobel_kernel_x[kernel_row][kernel_col]);
			coef_y = vdupq_n_s16(sobel_kernel_x[kernel_col][kernel_row]);
			acc_x = vmlaq_s16(acc_x, coef_x, neon_pixels);
			acc_y = vmlaq_s16(acc_y, coef_y, neon_pixels);

			kernel_col = 1;
			pixel_index = ((i+kernel_row) * src.cols) +j+kernel_col;
			neon_pixels = vreinterpretq_s16_u16(vmovl_u8(vld1_u8(data + pixel_index)));
			coef_x = vdupq_n_s16(sobel_kernel_x[kernel_row][kernel_col]);
			coef_y = vdupq_n_s16(sobel_kernel_x[kernel_col][kernel_row]);
			acc_x = vmlaq_s16(acc_x, coef_x, neon_pixels);
			acc_y = vmlaq_s16(acc_y, coef_y, neon_pixels);

			kernel_col = 2;
			pixel_index = ((i+kernel_row) * src.cols) +j+kernel_col;
			neon_pixels = vreinterpretq_s16_u16(vmovl_u8(vld1_u8(data + pixel_index)));
			coef_x = vdupq_n_s16(sobel_kernel_x[kernel_row][kernel_col]);
			coef_y = vdupq_n_s16(sobel_kernel_x[kernel_col][kernel_row]);
			acc_x = vmlaq_s16(acc_x, coef_x, neon_pixels);
			acc_y = vmlaq_s16(acc_y, coef_y, neon_pixels);

			kernel_row = 1;
			kernel_col = 0;
			pixel_index = ((i+kernel_row) * src.cols) +j+kernel_col;
			neon_pixels = vreinterpretq_s16_u16(vmovl_u8(vld1_u8(data + pixel_index)));
			coef_x = vdupq_n_s16(sobel_kernel_x[kernel_row][kernel_col]);
			coef_y = vdupq_n_s16(sobel_kernel_x[kernel_col][kernel_row]);
			acc_x = vmlaq_s16(acc_x, coef_x, neon_pixels);
			acc_y = vmlaq_s16(acc_y, coef_y, neon_pixels);

			/* kernel_col 1 skipped as coef = 0 */
			kernel_col = 2;
			pixel_index = ((i+kernel_row) * src.cols) +j+kernel_col;
			neon_pixels = vreinterpretq_s16_u16(vmovl_u8(vld1_u8(data + pixel_index)));
			coef_x = vdupq_n_s16(sobel_kernel_x[kernel_row][kernel_col]);
			coef_y = vdupq_n_s16(sobel_kernel_x[kernel_col][kernel_row]);
			acc_x = vmlaq_s16(acc_x, coef_x, neon_pixels);
			acc_y = vmlaq_s16(acc_y, coef_y, neon_pixels);

			kernel_row = 2;
			kernel_col = 0;
			pixel_index = ((i+kernel_row) * src.cols) +j+kernel_col;
			neon_pixels = vreinterpretq_s16_u16(vmovl_u8(vld1_u8(data + pixel_index)));
			coef_x = vdupq_n_s16(sobel_kernel_x[kernel_row][kernel_col]);
			coef_y = vdupq_n_s16(sobel_kernel_x[kernel_col][kernel_row]);
			acc_x = vmlaq_s16(acc_x, coef_x, neon_pixels);
			acc_y = vmlaq_s16(acc_y, coef_y, neon_pixels);

			kernel_col = 1;
			pixel_index = ((i+kernel_row) * src.cols) +j+kernel_col;
			neon_pixels = vreinterpretq_s16_u16(vmovl_u8(vld1_u8(data + pixel_index)));
			coef_x = vdupq_n_s16(sobel_kernel_x[kernel_row][kernel_col]);
			coef_y = vdupq_n_s16(sobel_kernel_x[kernel_col][kernel_row]);
			acc_x = vmlaq_s16(acc_x, coef_x, neon_pixels);
			acc_y = vmlaq_s16(acc_y, coef_y, neon_pixels);

			kernel_col = 2;
			pixel_index = ((i+kernel_row) * src.cols) +j+kernel_col;
			neon_pixels = vreinterpretq_s16_u16(vmovl_u8(vld1_u8(data + pixel_index)));
			coef_x = vdupq_n_s16(sobel_kernel_x[kernel_row][kernel_col]);
			coef_y = vdupq_n_s16(sobel_kernel_x[kernel_col][kernel_row]);
			acc_x = vmlaq_s16(acc_x, coef_x, neon_pixels);
			acc_y = vmlaq_s16(acc_y, coef_y, neon_pixels);

			/*store */
			pixel_index = (i+1)*src.cols + (j+1);

			acc_x = vqabsq_s16(acc_x);
			acc_y = vqabsq_s16(acc_y);

			uint16x8_t sum = vaddq_u16(vreinterpretq_u16_s16(acc_x), vreinterpretq_u16_s16(acc_y));

			uint8x8_t val = vqrshrn_n_u16(sum, 1);

			vst1_u8(dst.data + pixel_index, val);//store into this
#endif
}


void sobel_neon(const Mat& src, Mat& dst)
{
	if (src.cols <8)
		sobel(src, dst);

	if (src.rows < 2)
		return;

	for (int i = 0; i <src.rows-2; i++) {
		for (int j = 0; j < src.cols-2-8; j+=8) {
			__sobel_neon(src, dst, i, j);
		}

		/* leftover columns */
		if ((src.cols-2-8)%8) {
			__sobel_neon(src, dst, i, (src.cols-2-8));
		}
	}

	//todo fill in edges
}

#include "main.h"

// Prasanta Samal , Dumitru,Houston 

using namespace cv;

//declarations LUTs wrt width, height; definitions made elsewhere below
//LUT_w and LUT_h are external variables for functions from this point on,
//to end of this file (ibid. re. scope); not visible in other files main.cpp, etc
Mat LUT_w;
Mat LUT_h;
#define min(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a < _b ? _a : _b; })


// Helper function
float sf(int in){
	if (in == 0)
		return 0.70710678118; // = 1 / sqrt(2)
	return 1.;
}

//matrix multiplication
void cmm(Mat lmf, Mat rmf, float* prod_ptr, int sbs)
{//caller must insure that matrix product lmf*rmf fits into reserved space pointed to by prod_ptr
    //lmf, rmf = left, right matrix factors; their sizes are well defined, per Mat objects
    // sbs=size of sub-block (which must be square)
    int lmf_w=lmf.cols;
    int lmf_h=lmf.rows;
    int rmf_w=rmf.cols;
    int rmf_h=rmf.rows;

    assert(lmf_w==rmf_h);
    assert(lmf_w%sbs==0 && lmf_h%sbs==0 &&  rmf_w%sbs==0 && rmf_h%sbs==0);
    int steps=lmf_w/sbs;
    //Mat prod=Mat(lmf_h,rmf_w,CV_32FC1);
    //float* prod_ptr  = prod.ptr<float>();
    float* lmf_ptr  = lmf.ptr<float>();
    float* rmf_ptr  = rmf.ptr<float>();

    for(int x = 0; x < lmf_h; x++) // `v,' or DCT-row-frequency-index
        {
            for(int y = 0; y < rmf_w; y++) // u or DCT-col-frequency-index
            {
                float value = 0.f;
                for(int i=0;i<steps;i++)
                {
                    value += lmf_ptr[x * lmf_w + i]//S[i,j]=S[image-row-index (ordinate, `y'),image-col-index (abscissa, `x')]
                    *rmf_ptr[i * rmf_w  + y];//Cw[j,y]=Cw[S-col-index,DCT-col-index]
                }

                prod_ptr[x * rmf_w + y] = value;//F[DCT-row-frequency-index (`v'), DCT-col-frequency-index (`u')]
            }
        }
        //return prod;
}


//block matrix multiplication
void bcmm(Mat lmf, Mat rmf, float* prod_ptr, int b)
{//caller must insure that matrix product lmf*rmf fits into reserved space pointed to by prod_ptr
    //lmf, rmf = left, right matrix factors; their sizes are well defined, per Mat objects
    // b=size of sub-block (which must be square)
    int A_cols=lmf.cols;
    int A_rows=lmf.rows;
    int B_cols=rmf.cols;
    int B_rows=rmf.rows;
    float value=0.f;

    assert(A_cols==B_rows);//multiplication is well-defined
    //assert(A_cols%b==0 && A_rows%b==0 &&  B_cols%b==0 && B_rows%b==0);

    float* A_ptr  = lmf.ptr<float>();
    float* B_ptr  = rmf.ptr<float>();

    for(int I = 0; I < A_rows; I+=b)
        {
            for(int J = 0; J < B_cols; J+=b)
                {
                    //value=0.f;
                    for(int K = 0; K < A_cols; K+=b)
                        {

                            for(int i = I; i <= min((I+b-1),A_rows); i++)
                                {
                                    for(int j = J; j <= min((J+b-1),B_cols); j++)
                                        {
                                            value=0.f;
                                            for(int k = K; k <= min((K+b-1),B_rows); k++)
                                                {
                                                    value+=A_ptr[i*A_cols+k]*B_ptr[k*B_cols+j];
                                                    //printf("\n\r  value= %f; k= %d\n\r",value,k);

                                                }
                                                prod_ptr[i*B_cols+j]+=value;
                                                //printf("\n\r  prod_ptr[%d * %d+%d]= %f; K= %d\n\r",i,B_cols,j,value,K);
                                        }
                                }
                        }
                }
        }
        //return prod;
}


// Initialize LUT
void initDCT(int WIDTH, int HEIGHT)
{





	// TODO
	float SCALEh = sqrt(2./HEIGHT);//sqrt(2.)/sqrt(HEIGHT);
    float SCALEw = sqrt(2./WIDTH);//sqrt(2.)/sqrt(WIDTH);

	//externals defined here, type float, but not initialized
	LUT_h=Mat(HEIGHT,HEIGHT,CV_32FC1);
	LUT_w=Mat(WIDTH,WIDTH,CV_32FC1);
/*
	float* ph;
	float* pw;
	ph=LUT_h.ptr<float>();
	pw=LUT_w.ptr<float>();
*/

	for(int i = 0; i < HEIGHT; i++) //row
    {
        float* Mi=LUT_h.ptr<float>(i);
        for(int x = 0; x < HEIGHT; x++) // column
        {
            // --- Store cos calculation by LUT ---
            //Mi[x]=(float) cos(M_PI/((float)HEIGHT)*(i+1./2.)*(float)x);
            //LUT_h.at<float>(i,x) = cos(M_PI/((float)HEIGHT)*(i+1./2.)*(float)x);
            *(Mi+x)=(float) cos(M_PI/((float)HEIGHT)*(i+1./2.)*(float)x);
            Mi[x]*=SCALEh*sf(x);
        }
    }

    for(int i = 0; i < WIDTH; i++) //row
    {
        float* Mi=LUT_w.ptr<float>(i);
        for(int x = 0; x < WIDTH; x++) // column
        {
            // --- Store cos calculation by LUT ---
            //Mi[x]=(float) cos(M_PI/((float)WIDTH)*(i+1./2.)*(float)x);
            //LUT_w.at<float>(i, x) = cos(M_PI/((float)WIDTH)*(i+1./2.)*(float)x);
            *(Mi+x)=(float) cos(M_PI/((float)WIDTH)*(i+1./2.)*(float)x);
            Mi[x]*=SCALEw*sf(x);
        }
    }




}



// Baseline: O(N^4)
/*
Mat student_dct(Mat input)
{
	const int HEIGHT = input.rows;
	const int WIDTH  = input.cols;
	printf("HEIGHT: %d\n\r",
				HEIGHT);

	float scale = 2./sqrt(HEIGHT*WIDTH);

	Mat result = Mat(HEIGHT, WIDTH, CV_32FC1);

	// Note: Using pointers is faster than Mat.at<float>(x,y)
	// Try to use pointers for your LUT as well
	float* result_ptr = result.ptr<float>();
	float* input_ptr  = input.ptr<float>();
	float* lut_h_ptr = LUT_h.ptr<float>();
	float* lut_w_ptr = LUT_w.ptr<float>();

	for(int x = 0; x < HEIGHT; x++) // `v,' or DCT-row-frequency-index
	{
		for(int y = 0; y < WIDTH; y++) // u or DCT-col-frequency-index
		{
			float value = 0.f;

			for(int i = 0; i < HEIGHT; i++)
			{
				for(int j = 0; j < WIDTH; j++)
				{
					value += input_ptr[i * WIDTH + j]//S[i,j]=S[image-row-index (ordinate, `y'),image-col-index (abscissa, `x')]
						// TODO
						// --- Replace cos calculation by LUT ---
						//* cos(M_PI/((float)HEIGHT)*(i+1./2.)*(float)x)
						//* cos(M_PI/((float)WIDTH)*(j+1./2.)*(float)y);
						//* (*(LUT_h.data+LUT_h.step[0]*i+LUT_h.step[1]*x))//LUT_h(i,x)
						//* (*(LUT_w.data+LUT_w.step[0]*j+LUT_w.step[1]*y));//LUT_w(j,y)
						//* LUT_h.at<float>(i,x)
						//* LUT_w.at<float>(j,y);
						*lut_h_ptr[i * HEIGHT + x] //Ch[i,x]=Ch[S-row-index,DCT-row-index]
						*lut_w_ptr[j * WIDTH  + y];//Cw[j,y]=Cw[S-col-index,DCT-col-index]
				}
			}
			// TODO
			// --- Incorporate the scale in the LUT coefficients ---
			// --- and remove the line below ---
			//value = scale * sf(x) * sf(y) * value;

			//insert into DCT matrix F
			result_ptr[x * WIDTH + y] = value;//F[DCT-row-frequency-index (`v'), DCT-col-frequency-index (`u')]
		}
	}

	return result;
}
*/


/*
// *****************
//   Hint
// *****************
//
// DCT as matrix multiplication


Mat student_dct(Mat input)
{
	// -- Works only for WIDTH == HEIGHT
	//assert(input.rows == input.cols);

	// -- Matrix multiply with OpenCV
	//Mat output = LUT_w * input * LUT_w.t();


	int WIDTH=input.cols;
    int HEIGHT=input.rows;

	Mat output=Mat(HEIGHT,WIDTH,CV_32FC1);
	Mat prod=Mat(HEIGHT,WIDTH,CV_32FC1);
	Mat temp=Mat(HEIGHT,WIDTH,CV_32FC1);
    //float* prod_ptr=prod.ptr<float>();
    float* temp_ptr=temp.ptr<float>();
    float* output_ptr=output.ptr<float>();
    cmm(input,LUT_w,temp_ptr,1);
    cmm(LUT_h.t(),temp,output_ptr,1);

	// TODO
	// Replace the line above by your own matrix multiplication code
	// You can use a temp matrix to store the intermediate result

	return output;
}
*/


//
// DCT as block matrix multiplication


Mat student_dct(Mat input)
{
	// -- Works only for WIDTH == HEIGHT
	//assert(input.rows == input.cols);

	// -- Matrix multiply with OpenCV
	//Mat output = LUT_w * input * LUT_w.t(); //ERROR HERE AND IN TEXT; TRANSPOSE IS ON WRONG SIDE


	int WIDTH=input.cols;
    int HEIGHT=input.rows;

	Mat output=Mat(HEIGHT,WIDTH,CV_32FC1);
	Mat prod=Mat(HEIGHT,WIDTH,CV_32FC1);
	Mat temp=Mat(HEIGHT,WIDTH,CV_32FC1);
    //float* prod_ptr=prod.ptr<float>();
    float* temp_ptr=temp.ptr<float>();
    float* output_ptr=output.ptr<float>();
    bcmm(input,LUT_w,temp_ptr,64);//block matrix multiplication w/block size 64-by-64
    bcmm(LUT_h.t(),temp,output_ptr,64);

	// TODO
	// Replace the line above by your own matrix multiplication code
	// You can use a temp matrix to store the intermediate result

	return output;
}











﻿//
//  main.cpp
//  Lab2
//
//  Created by Alireza on 7/6/17.
//  Copyright © 2017 Alireza. All rights reserved.
//

#include <stdlib.h>
#include <fcntl.h>
#include <iostream>
#include <stdio.h>
#include <string>
#include <sstream>
#include <pthread.h>

using namespace std;

/* configure the led gpios as outputs */ 
void init_led(int led){
	ostringstream led_s;
	led_s << (led+57+4);
	string name = "/sys/class/gpio/gpio";
	name.append(led_s.str());
	if(access(name.c_str(), F_OK) == -1){
		string str1 = "echo ";
		str1.append(led_s.str());
		str1.append(" > /sys/class/gpio/export");
		system(str1.c_str());

		string str2 = "echo out > /sys/class/gpio/gpio";
		str2.append(led_s.str());
		str2.append("/direction");
		system(str2.c_str());
	} else {
		cout << "led_" << led_s.str() << " is already initialized." << endl;
	}
}

/* set the led on/off */
void led_state(int led, int v){
	ostringstream led_s;
	led_s << (led+57+4);
	ostringstream v_s;
	v_s << (v);
	string str = "echo ";
	str.append(v_s.str());
	str.append(" > /sys/class/gpio/gpio");
	str.append(led_s.str());
	str.append("/value");
	system(str.c_str());
}
//all structures below are global to functions that follow them, hence shared by all threads
//declaration w/o initialization (makes it available for decl. below)
struct philosopher;

struct fork {//resource
	struct philosopher* owner;//points to an owner, which is a philosopher
};

struct fork_manager {//struct. w/resource and its lock 
	struct fork fork;//each resource (fork) is a structure with pointer to current owner
	pthread_mutex_t access;// its mutex variable
};

//phil. has its own LED, and pointers to resources in order of acquisition 
//prevents deadlock (phil. do not all pick first a fork from same side)
struct philosopher {
	struct fork_manager *lesser_fork;
	struct fork_manager *greater_fork;
	int led;
};

/* this has to be provided from command line */
//globals, shared by threads
static int min_eat;
static int max_eat;
static int min_think;
static int max_think;

static inline void msleep (int ms) {
	usleep(ms * 1000);
}

/* generate an independent uniform random number between min & max */
int generate_random(int min, int max)
{
	
	unsigned long long tmp;

	tmp = rand();
	tmp = tmp * (max-min);
	tmp = tmp / RAND_MAX;
	tmp = tmp + min;
	return (int)tmp;

// alternate scaling algorithm
//	return (rand()%(max-min)) + min;
}

//wait on the fork to be available and grab it
//me is the philosopher whose thread is running
void acquire_fork(struct fork_manager *manager, struct philosopher *me)
{
	//wait for the fork to be available and claim it
	pthread_mutex_lock(&manager->access);//pass address of lock (cf. operator precedence)

	//check that the fork is unowned otherwise there is a bug
	if (manager->fork.owner != NULL)//must be NULL if it's passed mutex_lock
		cout << "acquire_fork bug" << endl;

	//pick up the fork by assigning it to oneself
	manager->fork.owner = me;
}

void release_fork(struct fork_manager *manager, struct philosopher *me)
{
	//confirm that no one stole the fork
	if (manager->fork.owner != me)
		cout << "release_fork bug" << endl;

	manager->fork.owner = NULL;//NULL pointer (to an owner) means `available'
	pthread_mutex_unlock(&manager->access);//release lock //let other philosophers observe the fork is available.
}

void philosopher_think(struct philosopher* me)
{
	int think_time = generate_random(min_think, max_think);

	printf("l%d think %d\n", me->led, think_time);
	//turn on my led;
	led_state(me->led, 1);//set LED state to 1
	msleep(think_time);//emulates thinking process
}

void philosopher_wait(struct philosopher* me)
{
	printf("l%d wait0\n",me->led);
	led_state(me->led, 0);//set LED state to 0
	// at least 1 philosopher shall 1st pick fork on right when all others have 1st picked left
	printf("l%d wait1\n",me->led);
	acquire_fork(me->lesser_fork, me);
	acquire_fork(me->greater_fork, me);
}

void philosopher_eat(struct philosopher* me)
{
	printf("l%d eat\n",me->led);
	int eat_time = generate_random(min_eat, max_eat); 

	int blink_on_time = 75; //ms
	int blink_off_time = 75; //ms

	//was waiting(off) so LED should transition to on first
	bool next_state_is_on = true;
	int next_time = blink_on_time;

	while (eat_time > next_time) {
		eat_time-=next_time;
		if (next_state_is_on) {
			led_state(me->led, 1);
			msleep(blink_on_time);
			next_state_is_on = false;
			next_time = blink_off_time;
		} else {
			led_state(me->led, 0);
			msleep(blink_off_time);
			next_state_is_on = true;
			next_time = blink_off_time;
		}
	}

	if (next_state_is_on) {
		led_state(me->led, 1);
		msleep(eat_time);//pass remaining eat time in `on' LED state
	} else {
		led_state(me->led, 0);
		msleep(eat_time);//pass remaining eat time in `off' LED state
	}

	release_fork(me->lesser_fork, me);
	release_fork(me->greater_fork, me);
}
//routine called when thread is created
void* philosopher_run(void* argument){
	struct philosopher *me = (struct philosopher*)argument;
	int led =  me->led;
	init_led(led);

	while(true)
	{
		philosopher_think(me);
		philosopher_wait(me);
		philosopher_eat(me);
	}
}

int main(int argc, const char * argv[])
{
	pthread_t tid[4];
	struct philosopher eaters[4] = {};
	struct fork_manager forks[4] = {};
	int tmp[4];

	for (int i = 0; i < 4; i++) {
		if(sscanf(argv[i+1], "%d", &tmp[i]) != 1) {
			cout << "parse_error" << endl;
			return -1;
		}
	}
	//read command line arguments
	min_eat = tmp[0];
	max_eat = tmp[1];
	min_think = tmp[2];
	max_think = tmp[3];
	printf("eat min %d, max %d; think min %d, max %d; ulong %d\n", min_eat, max_eat, min_think, max_think, sizeof(unsigned long));


	for (int i = 0; i < 4; i++) {
		forks[i].fork.owner = NULL;
		pthread_mutex_init(&forks[i].access, NULL);
	}
	
	//arrangement is as below
	//fork0 eater0 fork1 eater1 fork2 eater2 fork3 eater3 (prevents deadlock)
	//at least 1 philosopher (#3) shall 1st pick fork on right when others have 1st picked left
	eaters[0].lesser_fork = &forks[0];
	eaters[0].greater_fork = &forks[1];
	eaters[0].led = 0;

	eaters[1].lesser_fork = &forks[1];
	eaters[1].greater_fork = &forks[2];
	eaters[1].led = 1;

	eaters[2].lesser_fork = &forks[2];
	eaters[2].greater_fork = &forks[3];
	eaters[2].led = 2;

	eaters[3].lesser_fork = &forks[0];
	eaters[3].greater_fork = &forks[3];
	eaters[3].led = 3;

	//create threads
	for (int i = 0; i < 4; i++) {
		int tmp = pthread_create(&(tid[i]), NULL, &philosopher_run,
					 (void*)&eaters[i]);
		if(tmp!= 0) {//check successful thread creation
			cout << "cannot create thread " << (i+1) << '.' << endl;
			return 0;
		}
	}
	//ensure that main() doesn't terminate prematurely
	pthread_join(tid[0], NULL);
	pthread_join(tid[1], NULL);
	pthread_join(tid[2], NULL);
	pthread_join(tid[3], NULL);

	for (int i = 0; i < 4; i++) {
		forks[i].fork.owner = NULL;
		pthread_mutex_destroy(&forks[i].access);
	}

	return 0;
}


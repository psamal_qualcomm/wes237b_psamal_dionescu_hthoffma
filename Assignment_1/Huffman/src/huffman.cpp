#include "huffman.h"

#include <vector>
#include <queue>
#include <cassert>
#include <stdexcept>
#include <stdlib.h>
#include <stdio.h>

#include <iostream>
using namespace std;

#define printf(...)

struct HuffmanTree {
	unsigned char character;
	unsigned int cfreq;
	struct HuffmanTree* zero_node;
	struct HuffmanTree* one_node;
	struct HuffmanTree* parent_node;
	bool hint;
	HuffmanTree(char character, unsigned int cfreq,
        struct HuffmanTree *zero_node = NULL,
        struct HuffmanTree *one_node = NULL) :
            character(character), cfreq(cfreq), zero_node(zero_node), one_node(one_node)
    {
        	this->hint = false;
		this->parent_node = NULL;
		if (zero_node != NULL) {
			zero_node->parent_node = this;
			zero_node->hint = false;
		}
		if (one_node != NULL) {
			one_node->parent_node = this;
			one_node->hint = true;
		}
	}
	~HuffmanTree() {
		delete zero_node, delete one_node;
	}
	class Compare {
	public:
		bool operator()(HuffmanTree *a, HuffmanTree *b)
		{
			return a->cfreq > b->cfreq;
		}
	};
};

void print_bits(vector<bool> &bits)
{
    int count = 0;
    for (vector<bool>::iterator it = bits.begin(); it!=bits.end(); it++)
    {
        if (*it)
            printf("1");
        else printf("0");
        count++;
        if(count == 4) {
            printf(" ");
            count = 0;
        }
    }
}

/* freq should have at least one count in it */
struct HuffmanTree* build_tree(unsigned int* freq)
{
	priority_queue<HuffmanTree *, vector<HuffmanTree *>, HuffmanTree::Compare > nodes;
	char dummy_char = 0;
	for (int character = 0; character < 256; character++)
		if (freq[character] > 0)
			nodes.push(new HuffmanTree(character, freq[character]));

	if (nodes.size() == 1)
		nodes.push(new HuffmanTree(dummy_char, 0));

	while (nodes.size() > 1) {
		HuffmanTree *zero, *one;
		zero = nodes.top();
		nodes.pop();
		one = nodes.top();
		nodes.pop();
		nodes.push( new HuffmanTree(dummy_char, zero->cfreq + one->cfreq, zero, one));
	}

	/* if freq is all 0s, nodes will be empty and this will bug */
	return nodes.top();
}

void __construct_encoder(HuffmanTree* encoder_tree, vector<bool> **encoder, vector<bool> &current_code)
{
	/* huffman tree doesn't have null leafs */
	if (encoder_tree->zero_node == NULL) {
		encoder[encoder_tree->character] = new vector<bool>(current_code);
		return;
	}

	current_code.push_back(false);
	__construct_encoder(encoder_tree->zero_node, encoder, current_code);
	current_code.pop_back();
	current_code.push_back(true);
	__construct_encoder(encoder_tree->one_node, encoder, current_code);
	current_code.pop_back();
}

void construct_encoder(HuffmanTree* encoder_tree, vector<bool> **encoder)
{
	vector<bool> current_code;

	/* take care of single character input */
	if (encoder_tree->zero_node == NULL) {
		current_code.push_back(false);
		encoder[encoder_tree->character] = new vector<bool>(current_code);
	}

	/* start recursion */
	__construct_encoder(encoder_tree->zero_node, encoder, current_code);
}

void save_character(vector <bool> &data, struct HuffmanTree *node)
{
	unsigned char character = node->character;

	for (int i = 0; i < 8; i++) {
		data.push_back(!!((1<<i) & character));
	}
}

struct HuffmanTree* restore_character(vector <bool>::iterator &it, vector<bool> &data)
{
	char tmp = 0;
	for (int i = 0; i < 8; i++) {
		if (*it == true)
			tmp |= (1<<i);
		++it;
	}

    /* frequency info was only needed durring original construction of the tree */
	return new HuffmanTree(tmp, 0);
}

#define parent_node_encoding false
#define leaf_node_encoding true
/*  */
int huffman_tree_save (vector <bool> &data, struct HuffmanTree *node)
{
	 /* parrent nodes in the huffman tree have both children. */
     if (node->zero_node != NULL) {
		data.push_back(parent_node_encoding);
		huffman_tree_save (data, node->zero_node);
		huffman_tree_save (data, node->one_node);
	} else {
		data.push_back(leaf_node_encoding);
		save_character (data, node);
	}
}

/* */
struct HuffmanTree* huffman_tree_restore (vector <bool>::iterator &it, vector<bool> &data)
{
	int status;
	unsigned char dummy_char = 'P';
	unsigned int dummy_freq = 0;

	if (it == data.end()) {
	/*	printf("%s: end of data reached", __func__); */
		throw std::out_of_range("No more bits in tree");
	}
	 /* parrent node encoded as false */
     if (*it == parent_node_encoding) {
		it++;
		return new HuffmanTree(dummy_char, dummy_freq,
			huffman_tree_restore (it, data),
			huffman_tree_restore (it, data));
	} else {
		it++;
		return restore_character (it, data);
	}
}


/* the encoder should be an empty array of 256 tree node pointers */
void construct_encoder(struct HuffmanTree *tree, struct HuffmanTree** encoder)
{
	if (tree->zero_node == NULL)
		encoder[tree->character] = tree;
	else {
		construct_encoder(tree->zero_node, encoder);
		construct_encoder(tree->one_node, encoder);
	}
}

void encode_char_recursive(vector<bool> &out, struct HuffmanTree* decoder_node)
{
	if (decoder_node->parent_node != NULL) {
		encode_char_recursive(out, decoder_node->parent_node);
		out.push_back(decoder_node->hint);
	}
}

static inline void encode_char(unsigned char character, vector<bool> &out, struct HuffmanTree** decoder)
{
	encode_char_recursive(out, decoder[character]);
}

void encode_text(const unsigned char *bufin, unsigned int bufinlen, vector<bool> &out, struct HuffmanTree** encoder)
{
	for (int i=0; i < bufinlen; i++)
		encode_char(bufin[i], out, encoder);
}

int decode_character(vector<bool>::iterator &in_it, vector<bool>::iterator in_it_end, const struct HuffmanTree *curr_node)
{
	struct HuffmanTree *next_node;

	if (curr_node->zero_node == NULL)
		return curr_node->character;

	if (in_it == in_it_end)
		 throw std::out_of_range("No more bits to finish character");

	if (*in_it)
		next_node = curr_node->one_node;
	else
		next_node = curr_node->zero_node;

	in_it++;
	return decode_character(in_it, in_it_end, next_node);
}

/**
* Prints the tree nodes in breadth-first order
*/
void print_tree(HuffmanTree *t) {
    deque< pair<HuffmanTree *, int> > q;
    q.push_back(make_pair(t, 0));
    int curlevel = -1;
    while (!q.empty()) {
        HuffmanTree *parent = q.front().first;
        int level = q.front().second;
        q.pop_front();
        if (curlevel != level) {
            curlevel = level;
            cout << "Level " << curlevel << endl;
        }
        cout << parent->cfreq << " " << parent->character << endl;
        if (parent->zero_node)
            q.push_back(make_pair(parent->zero_node, level + 1));
        if (parent->one_node)
            q.push_back(make_pair(parent->one_node, level + 1));
    }
}

void decode_text(unsigned char **bufout, unsigned int *len, vector<bool>::iterator &in_it, vector<bool> &in, const struct HuffmanTree *root)
{
	vector<unsigned char> tmp;
	vector<unsigned char>::iterator tmp_it;
	int read_char = 0;
	unsigned char* tmp_array;

	while (in_it != in.end()) {
		read_char = decode_character(in_it, in.end(), root);
		tmp.push_back((unsigned char) read_char);
//		printf("%c",read_char);
	}

	*bufout = tmp_array = (unsigned char*) malloc(tmp.size());
	*len = tmp.size();
	tmp_it = tmp.begin();

	for (int i = 0; i < tmp.size() && tmp_it != tmp.end(); i++, tmp_it++)
		tmp_array[i] = *tmp_it;
}

char pack_char(vector<bool>::iterator &it, vector<bool> &data) {
    char ret = 0;

    for (int i = 0; i < 8; i++) {
        if (*it)
            ret |= 1<<i;
        it++;
        if (it == data.end())
            break;
    }
//    printf("%x\n", ret);
    return ret;
}

void unpack_char(vector<bool> &data, char c) {
    char ret = 0;

    for (int i = 0; i < 8; i++)
        data.push_back(c&(1<<i));
}

int huff_pad_and_store_as_char_array(unsigned char **pbufout,
						  unsigned int *pbufoutlen,
						  vector<bool> &out_bool)
{
    // add 1 for the padding length byte
    unsigned int len = out_bool.size()/8 +1;
    unsigned char padding=0;
    unsigned char *out;
    vector<bool>::iterator it = out_bool.begin();

    printf("encoded bits\n");
    print_bits(out_bool);
    printf("\n");

    printf("size of bool vector %d\n",out_bool.size());
    if (out_bool.size() %8)
    {
        len++;
        padding = 8 - (out_bool.size() %8);
    }

    out = (unsigned char*)malloc(len);
    printf("len %d\n", len);
    out[0] = padding;
    for (int i = 1; i<len; i++) {
        out[i] = pack_char(it,out_bool);
        if(it == out_bool.end()) {
            printf("reached end of iterator %d, %d\n", len, i);
        }
    }

    *pbufoutlen = len;
    *pbufout = out;
}

void huff_unpack_padded_char_array(const unsigned char *bufin,
						  const unsigned int bufinlen, vector<bool> &data)
{
    //vector<bool> data = new vector<bool>();
    unsigned char padding = bufin[0];

    for (int i = 1; i < bufinlen; i++)
        unpack_char(data, bufin[i]);

    for (int i = 0; i < padding; i++)
        data.pop_back();
}

/**
 * TODO Complete this function
 **/
int huffman_encode(const unsigned char *bufin,
						  unsigned int bufinlen,
						  unsigned char **pbufout,
						  unsigned int *pbufoutlen)
{
	unsigned int freq[256] = {0};
	struct HuffmanTree* encoder_tree;
    vector<bool> out_bool;

	if (bufinlen == 0) {
		*pbufout = (unsigned char*)malloc(1);
		*pbufoutlen = 0;
		return 0;
	}

	printf("%s: buflen %d\n",__func__, bufinlen);

	/* get the counts */
	for (unsigned int i = 0; i < bufinlen; i++)
		freq[bufin[i]]++;

	encoder_tree = build_tree(&freq[0]);

	struct HuffmanTree *encoder[256];

	construct_encoder(encoder_tree, encoder);

    print_tree(encoder_tree);

    huffman_tree_save(out_bool,encoder_tree);
    printf("1:size of bool vector %d\n",out_bool.size());
    encode_text(bufin, bufinlen, out_bool, encoder);
    printf("2:size of bool vector %d\n",out_bool.size());
    huff_pad_and_store_as_char_array(pbufout, pbufoutlen, out_bool);
    printf("done \n");
	return 0;
}


/**
 * TODO Complete this function
 **/
int huffman_decode(const unsigned char *bufin,
						  unsigned int bufinlen,
						  unsigned char **pbufout,
						  unsigned int *pbufoutlen)
{
    vector<bool> data;

    if (bufinlen == 0) {
	*pbufout = (unsigned char*) malloc(1);
	*pbufoutlen = 0;
	return 0;
    }
	

    huff_unpack_padded_char_array(bufin, bufinlen, data);

    printf("encoded bits parsed\n");
    print_bits(data);
    printf("\n");

    vector<bool>::iterator data_it = data.begin();
    struct HuffmanTree *decoder = huffman_tree_restore(data_it,data);
    print_tree(decoder);
    decode_text(pbufout,pbufoutlen,data_it,data,decoder);

	return 0;
}


/* int main()
{
    const unsigned char *bufin = (const unsigned char*)" In hac habitasse platea di";
    unsigned int bufinlen = sizeof("In hac habitasse platea di");
    unsigned int pbufoutlen = 0;
    unsigned char *pbufout = 0;
    huffman_encode(bufin,bufinlen,&pbufout,&pbufoutlen);

    printf("len = %d\n", pbufoutlen);
    for (int i=0; i<pbufoutlen; i++)
        printf("%2x\n",pbufout[i]);

    huffman_decode(pbufout, pbufoutlen, &pbufout, &pbufoutlen);

    printf("len = %d\n", pbufoutlen);
    for (int i=0; i<pbufoutlen; i++)
        printf("%c",pbufout[i]);

    printf("\n");
}*/

/*

int main(int argc, const char * argv[])
{
    cout << "WES237B Assignment 1\n";

	if(argc < 4)
	{
		cout << "Usage: " << argv[0] << " <input.txt> <code.txt> <output.txt>" << endl;
		return EXIT_FAILURE;
	}

	const char* in_filename   = argv[1];
	const char* code_filename = argv[2];
	const char* out_filename  = argv[3];

	// Read input
	ifstream in("/home/prasanta/Downloads/huffman/input.txt");

	string in_contents((istreambuf_iterator<char>(in)), istreambuf_iterator<char>());


	// Encode
	unsigned char* code = NULL;
	uint32_t code_size = 0;

	int ret;
	ret = huffman_encode((unsigned char*)in_contents.c_str(), in_contents.size(), &code, &code_size);

	if(ret != 0)
	{ cerr << "Huffman encode failed with code " << ret << endl; return EXIT_FAILURE; }


	// Save code to file
	if(code)
	{
		ofstream out_code(code_filename);
		for(uint32_t i = 0; i < code_size; i++)
		{
			out_code << code[i];
		}
	}


	// Read code from the same file
	ifstream in_code(code_filename);
	string code_contents((istreambuf_iterator<char>(in_code)), istreambuf_iterator<char>());


	// Decode
	unsigned char* decode = NULL;
	uint32_t decode_size = 0;

	ret = huffman_decode((unsigned char*)code_contents.c_str(), code_contents.size(), &decode, &decode_size);

	if(ret != 0)
	{ cerr << "Huffman decode failed with code " << ret << endl; return EXIT_FAILURE; }


	// Save output to file
	if(decode)
	{
		ofstream out(out_filename);
		out << (const char*)decode;
	}


	// Free memory
	if(code){ free(code); }
	if(decode){ free(decode); }


	// Check output
	string diff_command = "diff " + string(in_filename) + " " + string(out_filename);
	int diff_ret = system(diff_command.c_str());

	if(WEXITSTATUS(diff_ret) == 0)
	{
		cout << "SUCCESS" << endl;
		return EXIT_SUCCESS;
	}
	else
	{
		cout << "FAILURE" << endl;
		return EXIT_FAILURE;
	}


    return 0;
}
*/




****Assignment 1, WES237-B ****
***Contribution:  ***
Part 1: contribution 
Prasanta Samal & Houston Hoffman : worked on GNU radio , Prasanta designed the GNU radio blokes and answered the Question for Part -1 
Houston Hoffman : made the required changes to iir_filter_ffd_impl 
Dumitru : helped in developing the Python code 

Part 2 : contribution :
Prasanta samal & Dumitru : developed the Algorithm 
Houston Hoffman : did the software development 
Prasanta samal : wrote the test plan and verified the software implementation 
Dumitru : wrote the explanation for 
Prasanta Samal, Houston Hoffman, Dumitru Ionescu
Dumitru : provided the explanation for PArt 2 , 2.5 

****Part 1 ****
GNU Radio 
1.	When Refresh rate was default i.e. 15 the GUI does not respond, the CPU was 100% occupied. (Refresh rate: how often the Device computes FFT ) when we lowered the refresh rate to 5 the CPU occupancy came down to a lower rate and we could move the GUI .


2.	When we modified the GUI slider block to control CH0 (Frequency in RTL-SDR source) 
and changed the baseband frequency, we got ~3 second latency  on Audio Output.

3.	“aU” : audio underrun 
“uU” :  usrp underrun : (universal radio peripheral  underrun) 
“U”  :  underrun
“uO” :   Overrun audio
“aO” : audio overrun 
“uO” : usrp overrun
 “O” overrun 
We observed a pattern of:  aUaUaUaUaUOOOOOOOOOOOOOOOOOOOO

aUaUaU means samples has not yet reached Audio Sink, OOOOOOOOOOOOO samples are being pushed in excess of 44 Khz which is audio output (loss less output rate) 

4.	We have submitted the convert.py program to convert file to txt format 

5.	Modified file iir_filter_ffd is submitted in bitbucket 
Compared this output with our collected data in step 4, they are identical.

****Question 2  (Part -2)   ****

**Explanation  for Part 2.5**

*Explanation for Part 2.5* The Huffman coding we implemented is for symbol by symbol source coding, with known symbol probabilities (computed for each input, prior to source encoding). The Huffman coding tree (source coding with prefix codes) is implemented via a priority queue, which in turn was implemented in our code as a max heap; max refers to the fact that the node keys (character frequencies) are decreasing from the root node to the leaf nodes. 
We used C++ standard library priority queue class with its member functions for pushing, popping (e.g., two nodes with least key values when creating an internal node), etc., while constructing the Huffman code tree.
The input is analyzed to construct a character frequency table which is used in turn for creating the Huffman tree. In order to use files to convey the encoder output associated with a given input, we need to have a total number of bits (in the resulting encoder output) that is a multiple of eight bits.
The format for the encoder output has 4 components:
I)	The number of padded bits (this was represented with an 8 bit character (given the implementation, there are 4 bits available to represent a version flag if one wanted to enable a different version of the encoding scheme);
II)	Preordered traversal of the Huffman tree with parent nodes represented as a 0 bit and leaf nodes represented as a 1 bit followed by the character represented by that leaf node.
III)	Bit stream for the encoded text (prefix codewords representing the source encoding of the input text).  This was efficiently packed into a Boolean vector;
IV)	Padded bits to allow the data stream to be saved to a file of characters.
The representation of the Huffman tree was recommended by Wikipedia and takes (6*n)-1 bits to encode (n is the number of unique characters present in the text) in the general case (different for 0 & 1 unique character cases where the representation is still competitively small).  
Test Plan: For Huffman encoder/decoder Input to the Code : Sample I/P file that was provided by Lab Expected O/P : Check the O/P from Encoder Check the O/P from Decoder Input : Random txt file input Check the O/P from Encoder Check the O/P from Decoder Input : an empty file The code should through an exception saying no txt, cannot encode or decode Input : file with just ~ 2 to 3 character

#include <stdio.h>
#include <stdlib.h>

#include "matrixmul.h"
#include "cuda_error.h"

//assumption blockDim.x = blockDim.y = BLOCK_SIZE
#define BLOCK_SIZE 30 

//load a subblock located at x,y from unified memory "in" into shared memory address "out".
//the subblock has blockDim dimensions
//width is the width of the entire factor matrix pointed to by "in"

static inline __device__ void fill_shared(float* out, const float* in, uint base_y, uint base_x, uint width)
{//each thread must load only one value from one factor matrix and one value from the other
//base_y = row base index (top left corner)
//base_x = column base index (top left corner)

	
	uint inx_idx = base_x + threadIdx.y;//col
	
	uint iny_idx = base_y + threadIdx.x;//row
	
	
	out[threadIdx.y + threadIdx.x*blockDim.y] = in[inx_idx + iny_idx*width];
}

#ifdef DEBUG
static inline __device__ void check_shared(float* out, const float* in, uint base_y, uint base_x, uint width)
{
	uint tmpx = (blockDim.x - threadIdx.x-1);
	uint tmpy = (blockDim.y - threadIdx.y-1);
	uint inx_idx = base_x + tmpx;//col
	uint iny_idx = base_y + tmpy;//row

//	printf("%d, %d, from %d, %d \n", threadIdx.x, threadIdx.y, inx_idx, iny_idx);	
	if (out[tmpx + tmpy*blockDim.x] != in[inx_idx + iny_idx*width])
		printf("shared load failed from %d %d\n", iny_idx, inx_idx);
}
#endif

static inline __device__ void export_shared(float* out, float in, uint width)
{
	
	uint outx_idx = blockIdx.y*blockDim.y + threadIdx.y;//col
	
	uint outy_idx = (blockIdx.x*blockDim.x + threadIdx.x);//row
//	printf("%d, %d, \n", outy_idx, outx_idx);
	out[outx_idx + outy_idx*width] = in;
}

/*IT IS STILL POSSIBLE TO DO THE CACHE-OPTIMIZED BLOCK MULTIPLICATION; 
one-product-matrix-row-worth of data is needed to be stored in shared memory for
the partial accumulations in a product-strip-row     */
/* compute and acumulate one entry of one subblock in product matrix */

static inline __device__ void subblock_mm(float* a_subblock, float* b_subblock, float& acc)
{
	//calculating subblock element x,y
	for(int idx = 0; idx < BLOCK_SIZE; idx++) {
		
		acc += a_subblock[idx + threadIdx.x*BLOCK_SIZE]*b_subblock[threadIdx.y + idx*BLOCK_SIZE];
	}
}

/*EACH THREAD COMPUTES ONE ENTRY IN THE PRODUCT MATRIX, THUS (withIN THE SUBBLOCK WHERE said ENTRY IS LOCATED)
IT REPEATEDLY MULTIPLIES ONE SUBBLOCK ROW BY ONE SUBBLOCK COLUMN, then ACCUMULATES */


__global__ void block_mm_kernel(const float* A, const float* B, float* output, int M, int N) 
{//threads in a subblock of the product matrix collectivelly load into their shared memory the factor matrix' entries that they will be using on aggregate
	//__shared__ float a_subblock[BLOCK_SIZE*BLOCK_SIZE];
	//__shared__ float b_subblock[BLOCK_SIZE*BLOCK_SIZE];
	float acc = 0;

	for(int i = 0; i < M/BLOCK_SIZE; i++) 
	{//simultaneously indexes subblock coulmns in A and subblock rows in B
		__shared__ float a_subblock[BLOCK_SIZE*BLOCK_SIZE];
		__shared__ float b_subblock[BLOCK_SIZE*BLOCK_SIZE];
		//assume blockDim.y = BLOCK_SIZE

		fill_shared(a_subblock, A, blockIdx.x*BLOCK_SIZE, i*BLOCK_SIZE, M);
		
		//assume blockDim.X = BLOCK_SIZE

		fill_shared(b_subblock, B, i*BLOCK_SIZE, blockIdx.y*BLOCK_SIZE, N);
		__syncthreads();
#ifdef DEBUG
		check_shared(a_subblock, A, blockIdx.x*BLOCK_SIZE, blockIdx.y*BLOCK_SIZE, M);
		check_shared(b_subblock, B, blockIdx.x*BLOCK_SIZE,blockIdx.y*BLOCK_SIZE, N);
#endif
		/*FOR EACH SUBBLOCK PRODUCT, MULTIPLY-AND-ACCUMULATE one SUB-BLOCK-ROW threadIdx.y
		by  one SUB-BLOCK-COLUMN threadIdx.x THEN ACCUMULATE ACCROSS SUBBLOCKS after synchronizing the threads, see below */
		subblock_mm(a_subblock, b_subblock, acc);
		//  subblocks may  still be in use by other threads, hence need to synchronize threads here ...
		__syncthreads();
	}

	export_shared(output, acc, N);
}


inline int divup(int a, int b)
{
	if (a % b)
		return a / b + 1;
	else
		return a / b;
}

float run_mm_gpu(const float* A, const float* B, float* C, int M, int N)
{
	// Profiling
	float time_gpu;

	cudaEvent_t start, stop;
	CudaSafeCall(cudaEventCreate(&start));
	CudaSafeCall(cudaEventCreate(&stop));

	CudaSafeCall(cudaEventRecord(start, 0));
	
	dim3 dimGrid(divup(N, BLOCK_SIZE), divup(N, BLOCK_SIZE));
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);

	// Launch kernel 
	block_mm_kernel<<<dimGrid, dimBlock>>>(A, B, C, M, N);

	CudaCheckError();

	CudaSafeCall(cudaThreadSynchronize());

	CudaSafeCall(cudaEventRecord(stop, 0));
	CudaSafeCall(cudaEventSynchronize(stop));
	
	CudaSafeCall(cudaEventElapsedTime(&time_gpu, start, stop));

	// Free Memory
	CudaSafeCall(cudaEventDestroy(start));
	CudaSafeCall(cudaEventDestroy(stop));

	return time_gpu;
}

#include "filter.h"
#include "cuda_timer.h"

#include <iostream>
#define CHECK_INDEXES 1
using namespace std;

#ifdef try_shared_memory
__global__
void kernel_sobel_filter(const uchar * input_in, uchar * output, const uint height, const uint width)
{
	//indexing into original image
	int i_in = blockIdx.x * blockDim.x + threadIdx.x;
	int j_in = blockIdx.y * blockDim.y + threadIdx.y;

	//indexing into thread block shared memory
	int i_block = threadIdx.x + 1;
	int j_block = threadIdx.y + 1;
	__shared__ uchar input_block[(blockDim.y+2)][(blockDim.x + 2)];

	int me = threadIdx.y*threadIdx.x;
	me 
	input_block[

#if CHECK_INDEXES	
	if (i_in < width)
#endif
		input_block[j_block][i_block] = input_in[j_in*width + i_in];
	....
}
#else
__global__
void kernel_sobel_filter(const uchar * input, uchar * output, const uint height, const uint width)
{
	const int sobel_x[3][3] = {
		{-1, 0, 1},
		{-2, 0, 2},
		{-1, 0, 1}
	};
	const int sobel_y[3][3]  = {
		{-1, -2, -1},
		{0,   0,  0},
		{1,   2,  1}
	};

	// (x, y) are unique in the set of threads and span the image. 
	uint x = blockIdx.x * blockDim.x + threadIdx.x;
	uint y = blockIdx.y * blockDim.y + threadIdx.y;

	if (y>=1 && y < height -1)
	{
		if (x>=1 && x < width - 1)
		{

			const int pixel_x = (int) (
					(sobel_x[0][0] * input[x-1 + (y-1) * width]) + 
					(sobel_x[0][1] * input[x   + (y-1) * width]) + 
					(sobel_x[0][2] * input[x+1 + (y-1) * width]) +
					(sobel_x[1][0] * input[x-1 + (y  ) * width]) + 
					(sobel_x[1][1] * input[x   + (y  ) * width]) + 
					(sobel_x[1][2] * input[x+1 + (y  ) * width]) +
					(sobel_x[2][0] * input[x-1 + (y+1) * width]) + 
					(sobel_x[2][1] * input[x   + (y+1) * width]) + 
					(sobel_x[2][2] * input[x+1 + (y+1) * width])
					);
			const int pixel_y = (int) (
					(sobel_y[0][0] * input[x-1 + (y-1) * width]) + 
					(sobel_y[0][1] * input[x   + (y-1) * width]) + 
					(sobel_y[0][2] * input[x+1 + (y-1) * width]) +
					(sobel_y[1][0] * input[x-1 + (y  ) * width]) + 
					(sobel_y[1][1] * input[x   + (y  ) * width]) + 
					(sobel_y[1][2] * input[x+1 + (y  ) * width]) +
					(sobel_y[2][0] * input[x-1 + (y+1) * width]) + 
					(sobel_y[2][1] * input[x   + (y+1) * width]) + 
					(sobel_y[2][2] * input[x+1 + (y+1) * width])
					);

			float magnitude = sqrt((float)(pixel_x * pixel_x + pixel_y * pixel_y));

			if (magnitude < 0){ magnitude = 0; }
			if (magnitude > 255){ magnitude = 255; }

			output[x + y * width] = magnitude;
		}
	}
}
#endif

inline int divup(int a, int b)
{
	if (a % b)
		return a / b + 1;
	else
		return a / b;
}

/**
 * Wrapper for calling the kernel.
 */
void sobel_filter_gpu(const uchar * input, uchar * output, const uint height, const uint width)
{
	//const int size = height * width * sizeof(uchar);

	CudaSynchronizedTimer timer;


	// Launch the kernel
	const int grid_x = 32;
	const int grid_y = 32;

	dim3 grid(divup(width,grid_x), divup(height,grid_y), 1);
	dim3 block(grid_x, grid_y, 1);

	timer.start();
	kernel_sobel_filter<<<grid, block>>>(input, output, height, width);
	timer.stop();

	cudaDeviceSynchronize();

	float time_kernel = timer.getElapsed();
}


void sobel_filter_cpu(const uchar * input, uchar * output, const uint height, const uint width)
{
	const int sobel_x[3][3] = {
		{-1, 0, 1},
		{-2, 0, 2},
		{-1, 0, 1}
	};
	const int sobel_y[3][3]  = {
		{-1, -2, -1},
		{0,   0,  0},
		{1,   2,  1}
	};

	for (uint y = 1; y < height - 1; ++y)
	{
		for (uint x = 1; x < width - 1; ++x)
		{

			const int pixel_x = (int) (
					(sobel_x[0][0] * input[x-1 + (y-1) * width]) + 
					(sobel_x[0][1] * input[x   + (y-1) * width]) + 
					(sobel_x[0][2] * input[x+1 + (y-1) * width]) +
					(sobel_x[1][0] * input[x-1 + (y  ) * width]) + 
					(sobel_x[1][1] * input[x   + (y  ) * width]) + 
					(sobel_x[1][2] * input[x+1 + (y  ) * width]) +
					(sobel_x[2][0] * input[x-1 + (y+1) * width]) + 
					(sobel_x[2][1] * input[x   + (y+1) * width]) + 
					(sobel_x[2][2] * input[x+1 + (y+1) * width])
					);
			const int pixel_y = (int) (
					(sobel_y[0][0] * input[x-1 + (y-1) * width]) + 
					(sobel_y[0][1] * input[x   + (y-1) * width]) + 
					(sobel_y[0][2] * input[x+1 + (y-1) * width]) +
					(sobel_y[1][0] * input[x-1 + (y  ) * width]) + 
					(sobel_y[1][1] * input[x   + (y  ) * width]) + 
					(sobel_y[1][2] * input[x+1 + (y  ) * width]) +
					(sobel_y[2][0] * input[x-1 + (y+1) * width]) + 
					(sobel_y[2][1] * input[x   + (y+1) * width]) + 
					(sobel_y[2][2] * input[x+1 + (y+1) * width])
					);

			float magnitude = sqrt((float)(pixel_x * pixel_x + pixel_y * pixel_y));

			if (magnitude < 0){ magnitude = 0; }
			if (magnitude > 255){ magnitude = 255; }

			output[x + y * width] = magnitude;
		}
	}
}



